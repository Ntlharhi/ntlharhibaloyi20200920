package Framework;

import Domain.*;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TestSuiteManager {

    //Assign step
    public static List<TestStep> Steps = new ArrayList<TestStep>();
    public static TestStep testStep = new TestStep();



    public static TestSuite testSuite = new TestSuite();
    public static TestCase testCase = new TestCase();

    public static Screenshot screenshot = new Screenshot();
    public static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public Timestamp timestamp = new Timestamp(System.currentTimeMillis());


    public static String currentTestId;
    public static String currentStepId;
    public static String currentScreenshotId;


        public void SetTestCaseName(String name,int id)
        {
            currentTestId =Long.toString(timestamp.getTime());
            testCase = new TestCase();
            Steps = new ArrayList<TestStep>();

            testCase.Id=currentTestId;
            testCase.TestSuiteId="b01ae008-38b7-4a51-93c6-16d494e36907";
            testCase.Name=name;
            testCase.QC_ID=id;
            Date date = new Date();
            testCase.ExecutionTime=formatter.format(date);
            testCase.Browser= TestSuiteManager.testSuite.BrowserName.get(0);
            testCase.RunOrder="System.Collections.Generic.List`1[Katana.Framework.TestCase]";
        }


        public void SetStep(String description,String expected)
        {
            currentStepId =Long.toString(timestamp.getTime());
            testStep = new TestStep();
            testStep.Id=currentStepId;
            testStep.TestCaseId= currentTestId;
            testStep.StepNumber =Steps.size()+1;
            testStep.Description=description;
            testStep.Expected=expected;
        }


        public static void EndTest()
        {
            testCase.TestSteps =new ArrayList<TestStep>(Steps);

            for (TestStep step:testCase.TestSteps) {

                if (!step.Status) {
                    testCase.Status = false;
                    break;
                } else {
                    testCase.Status = true;
                }
            }
            TestSuiteManager.testSuite.TestCases.add(testCase);
        }


    public static  void EndSuite()
    {
        int passCount=0;
        int failCount;
        TestSuiteManager.testSuite.Status=true;

        for(TestCase test : TestSuiteManager.testSuite.TestCases)
        {
            if(test.Status)
            {
                passCount++;
            }else {
                TestSuiteManager.testSuite.Status=false;}
        }
        failCount= TestSuiteManager.testSuite.TestCases.size()-passCount;
        TestSuiteManager.testSuite.TotalFail=Integer.toString(failCount);
        TestSuiteManager.testSuite.TotalPass=Integer.toString(passCount);
        TestSuiteManager.testSuite.TestCaseCount= Integer.toString(TestSuiteManager.testSuite.TestCases.size());
        TestSuiteManager.testSuite.PassPercentage =Double.toString(passCount/ TestSuiteManager.testSuite.TestCases.size());
        TestSuiteManager.testSuite.FailPercentage =Double.toString(failCount/ TestSuiteManager.testSuite.TestCases.size());
        TestSuiteManager.testSuite.ExecutionDateTime=formatter.format(EnvironmentManager.startDateTime);
        TestSuiteManager.testSuite.ExecutionTime =formatter.format(EnvironmentManager.startDateTime);
    }


    public static void tearDown()
    {
        try {
            //Construct json date and calculations
            TestSuiteManager.EndSuite();
            //Copy Report template into Reports folder
            EnvironmentManager.CreateWebReport();

            //Create JSON File which holds the results
            EnvironmentManager.WriteJsonResultsToTheReport();

            //Zip the report
            String zipOutPut="src/test/Execution Reports/"+EnvironmentManager.currentReport+".zip";
            Report appZip = new Report();
            appZip.generateFileList(new File("src/test/Execution Reports/"+EnvironmentManager.currentReport));
            appZip.zipIt(zipOutPut);

            //Email the report to the relevant stakeholders
            if(Config.sendEmail)
            {
                Notification.sendEmail("IQ Business Automation Report","IQ Business Automation Report",zipOutPut);
            }

        } catch (Exception e) {

        }
    }

}
