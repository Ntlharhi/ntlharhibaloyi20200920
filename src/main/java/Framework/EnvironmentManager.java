package Framework;

import Domain.Config;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;


public class EnvironmentManager {
    public Logger logger = Logger.getLogger("Logs");
    FileHandler fh;
    public static HashMap<String,String> objectRepository = new HashMap<String, String>();
    public static int driverType;
    public static String environmentUrl;
    public static  List<String> browsers = new ArrayList<String>();
    public static Date startDateTime = new Date();
    public static Config config;
    public static String currentReport;
    public static String DriverPath;


    public  EnvironmentManager() {
        try
        {
            getConfiguration();

            environmentUrl=Config.Environment;
            DriverPath="src/main/java/WebDrivers/";

            TestSuiteManager.testSuite.EnvironmentName="QA";
            TestSuiteManager.testSuite.ProjectId="fc044084-4224-4f43-ab23-840f793b5936";
            TestSuiteManager.testSuite.Id= "b01ae008-38b7-4a51-93c6-16d494e36907";
            ObjectMapping();
        }
        catch (Exception e)
        {
        }

    }



    public void ObjectMapping()
    {
        try
        {
            BufferedReader bufReader = new BufferedReader(new FileReader("src/test/java/Mapping/objectMapping.txt"));
            ArrayList<String> listOfLines = new ArrayList<String>();

            String line = bufReader.readLine();
            while (line != null) {
                listOfLines.add(line);
                line = bufReader.readLine();
            }
            bufReader.close();

            for(String obj : listOfLines)
            {
                String[] arrOfStr =obj.split("->");
                objectRepository.put(arrOfStr[0],arrOfStr[1]);
            }
        }
        catch (Exception e)
        {

        }
    }


    public static void CreateWebReport()
    {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        currentReport="Automation_"+timestamp.getTime();
        File file = new File("src/test/Execution Reports/"+currentReport);
        if(file.mkdir())
        {
            try
            {
                FileUtils.copyDirectory(new File("src/main/java/Report"), new File("src/test/Execution Reports/"+currentReport));
            }
            catch (Exception e)
            {
               // logger.info("Experience an error creating the report :"+e);
            }
        }
    }

    public static void WriteJsonResultsToTheReport()
    {
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            //Generate JSON file for the report
            String jsonString = mapper.writeValueAsString(TestSuiteManager.testSuite);
            BufferedWriter writer = new BufferedWriter(new FileWriter("src/test/Execution Reports/"+EnvironmentManager.currentReport+"/Libraries/misc/data.js"));
            writer.write("data="+jsonString);
            writer.close();
        }
        catch (Exception e)
        {

        }
    }

    public static void getConfiguration()
    {
        try
        {
            JSONParser parser = new JSONParser();

            Object obj = parser.parse(new FileReader("src/main/java/Framework/Config/config.json"));

            // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
            JSONObject jsonObject = (JSONObject) obj;

            // A JSON array. JSONObject supports java.util.List interface.
            JSONArray companyList = (JSONArray) jsonObject.get("emails");

            Config.Environment=jsonObject.get("Environment").toString();
            Config.Browser=(List<String>) jsonObject.get("Browser");
            Config.emailUsername=jsonObject.get("emailUsername").toString();
            Config.emailPassword=jsonObject.get("emailPassword").toString();
            Config.emailHost=jsonObject.get("emailHost").toString();
            Config.emailPort=jsonObject.get("emailPort").toString();
            Config.emailFrom=jsonObject.get("emailFrom").toString();
            Config.emails=(List<String>) jsonObject.get("emails");
            Config.sendEmail=(Boolean)jsonObject.get("sendEmail");


        }
        catch (Exception e)
        {
            String aaa=e.getMessage();
        }
    }

}
