package Framework;

import Domain.Screenshot;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static Framework.EnvironmentManager.objectRepository;

public class Executor extends TestSuiteManager {

    public static WebDriver driver;

    public Executor()
    {
        EnvironmentManager env = new EnvironmentManager();

        objectRepository.put("lnkSignUp","//a[text()='Sign Up']");
        objectRepository.put("btnNext","//span[text()='NEXT']");
        objectRepository.put("btnTermsAndCondition","//label[text()='I agree to the ']");
        objectRepository.put("txtCompany","///input[@id='company']");
        objectRepository.put("txtJobTitle","//input[@id='job']");
        objectRepository.put("txtMobileNumber","//input[@id='mobile']");
        objectRepository.put("txtEmail","//input[@id='email1']");
        objectRepository.put("txtName","//input[@id='userName']");


        TestSuiteManager.testSuite.Name="LawTrust Test Scenarios";
    }


    public void NavigateTo(String browser,String url)
    {
        TestSuiteManager.testSuite.BrowserName.add(browser);
        if(browser.equals("Chrome"))
        {
            System.setProperty("webdriver.chrome.driver",EnvironmentManager.DriverPath+"chromedriver.exe");
            driver = new ChromeDriver();
        }

        if(browser.equals("FireFox"))
        {
            System.setProperty("webdriver.gecko.driver",EnvironmentManager.DriverPath+"geckodriver.exe");
             driver = new FirefoxDriver();
        }

        if(browser.equals("IE"))
        {
            System.setProperty("webdriver.ie.driver", EnvironmentManager.DriverPath+"IEDriverServer.exe");
             driver=new InternetExplorerDriver();
        }


        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
    }



public void ScrollBottom()
{
    JavascriptExecutor jsx = (JavascriptExecutor)driver;
    jsx.executeScript(("window.scrollBy(0,450);"));
}




    public void ReportItem(String objectMapped,String type,String failMessage)
    {
        if(objectRepository.containsKey(objectMapped))
        {
            try {

                WebElement element = Element(objectMapped, type);

                if (element.isDisplayed()) {
                    testStep.Status = true;
                    testStep.Actual=testStep.Expected;

                } else{
                    testStep.Status = false;
                    testStep.Actual=failMessage;
                }
                ReportScreenshots(element);
                Steps.add(testStep);
            }
            catch(Exception e)
            {

            }

        }
    }

    public void Click(String objectMapped,String type,String failMessage)
    {

        //EnvironmentManager en = new EnvironmentManager();
       // en.ObjectMapping();




        boolean ccee =objectRepository.containsKey(objectMapped);

        if(ccee)
        {
            try {

                WebElement element = Element(objectMapped, type);

                if (element.isDisplayed()) {
                    testStep.Status = true;
                    testStep.Actual=testStep.Expected;

                } else{
                    testStep.Status = false;
                    testStep.Actual=failMessage;
            }
                ReportScreenshots(element);
                element.click();

                Steps.add(testStep);
            }
            catch(Exception e)
            {

            }

        }
    }

    public void Click(String objectMapped,String type,boolean img)
    {
        if(objectRepository.containsKey(objectMapped))
        {
            try {

                WebElement element = Element(objectMapped, type);

                element.click();
            }
            catch(Exception e)
            {

            }

        }
    }


    public void ClearAndType(String objectMapped,String type,String value,String failMessage)
    {
        if(objectRepository.containsKey(objectMapped))
        {
            try
            {
                WebElement element =  Element(objectMapped,type);


                if(element.isDisplayed())
                {
                    element.clear();
                    element.sendKeys(value);
                    ReportScreenshots(element);

                    //set results
                    String elementValue =Element(objectMapped,type).getAttribute("value");
                    if(elementValue.contains(value))
                    {
                        testStep.Status =true;
                        testStep.Actual=testStep.Expected;
                    }
                    else
                    {
                        testStep.Status =false;
                        testStep.Actual=failMessage;
                    }

                    Steps.add(testStep);
                }
            }
            catch(Exception e)
            {

            }
        }
    }


    public  void ScrollToView( WebElement element)
    {
   JavascriptExecutor je =(JavascriptExecutor) driver;
   je.executeScript("arguments[0].scrollIntoView(true);",element);
    }




    public void ClearAndType(String objectMapped,String type,String value,boolean img)
    {
        if(objectRepository.containsKey(objectMapped))
        {
            try
            {
                WebElement element =  Element(objectMapped,type);
                element.clear();
                element.sendKeys(value);

                //set results
                String elementValue =Element(objectMapped,type).getAttribute("value");
                if(elementValue.contains(value))
                {
                }
                else
                {
                }
            }
            catch(Exception e)
            {

            }
        }
    }




    public void ReportScreenshots(WebElement element){
        screenshot = new Screenshot();
        currentScreenshotId=Long.toString(timestamp.getTime());

        String originalstyle =element.getAttribute("style");

        ((JavascriptExecutor)driver).executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 2px solid red;");


        screenshot.Screenshots = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
        screenshot.Id=currentScreenshotId;
        screenshot.TestStepId=currentTestId;
        testStep.Screenshot.add(screenshot);


        ((JavascriptExecutor)driver).executeScript("arguments[0].removeAttribute('style')", element);
        ((JavascriptExecutor)driver).executeScript("arguments[0].setAttribute('style', arguments[1]);", element, originalstyle);

    }
    public void ScrollToButtonOrText(String textName)
    {
        try
        {
            JavascriptExecutor js = (JavascriptExecutor)driver;
                js.executeScript("$(window).scrollTop($(\" *:contains('"+textName+"'):last\").offset().top);");
            js.executeScript("$('html, body').animate({scrollTop: $(window).scrollTop() - 250});");
        }
        catch (Exception e)
        {

        }
    }


    public void Clear(String objectMapped,String type,String failMessage)
    {
        if(objectRepository.containsKey(objectMapped))
        {
            try
            {
               Element(objectMapped,type).clear();
            }
            catch(Exception e)
            {

            }
        }
    }



    public void Type(String objectMapped,String type,String value)
    {
        if(objectRepository.containsKey(objectMapped))
        {
            try
            {
                WebElement element= Element(objectMapped,type);

                //set results
                element.sendKeys(value);

            }
            catch(Exception e)
            {

            }
        }
    }


    public WebElement Element(String object,String type)
    {
        WebElement element = null;
        try
        {
            if(type.contains("xpath")){

                String ccc=objectRepository.get(object);

                element= driver.findElement(By.xpath(ccc));
            }
        }
        catch (Exception e)
        {

        }






        if(type.contains("id")){element= driver.findElement(By.id(objectRepository.get((object))));}
        if(type.contains("css")){element= driver.findElement(By.cssSelector(objectRepository.get((object))));}
        if(type.contains("name")){element= driver.findElement(By.name(objectRepository.get((object))));}
        if(type.contains("linkText")){element= driver.findElement(By.linkText(objectRepository.get((object))));}
        if(type.contains("className")){element= driver.findElement(By.className(objectRepository.get((object))));}
        if(type.contains("tagName")){element= driver.findElement(By.tagName(objectRepository.get((object))));}
        return  element;
    }


    public List<WebElement> getElements(String object,String type)
    {
        List<WebElement> elements = null;
        if(type.contains("xpath")){elements= driver.findElements(By.xpath(objectRepository.get((object))));}
        if(type.contains("id")){elements= driver.findElements(By.id(objectRepository.get((object))));}
        if(type.contains("css")){elements= driver.findElements(By.cssSelector(objectRepository.get((object))));}
        if(type.contains("name")){elements= driver.findElements(By.name(objectRepository.get((object))));}
        if(type.contains("linkText")){elements= driver.findElements(By.linkText(objectRepository.get((object))));}
        if(type.contains("className")){elements= driver.findElements(By.className(objectRepository.get((object))));}
        if(type.contains("tagName")){elements= driver.findElements(By.tagName(objectRepository.get((object))));}
        return elements;
    }


}
