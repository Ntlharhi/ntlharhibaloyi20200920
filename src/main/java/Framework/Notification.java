package Framework;
import Domain.Config;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;


public class Notification {

    public static void sendEmail(String subj,String message,String attachmentName) {
        // Defines the E-Mail information.
        String subject = subj;
        String bodyText =message;

        // Creates a Session with the following properties.
        Properties props = new Properties();
        props.put("mail.host", Config.emailHost);
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.starttls.enable", "true");
        props.put("mail.port", Config.emailPort);
        Session session = Session.getDefaultInstance(props);

        try {
            InternetAddress fromAddress = new InternetAddress(Config.emailFrom);
            InternetAddress toAddress = new InternetAddress(Config.emails.get(0));

            // Create an Internet mail msg.
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(fromAddress);
            msg.setRecipient(Message.RecipientType.TO, toAddress);

            if(Config.emails.size()>1)
            {
                InternetAddress ccAddress ;
                for(String email:Config.emails)
                {
                    ccAddress = new InternetAddress(email);
                    msg.addRecipient(Message.RecipientType.CC, ccAddress);
                }
            }
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            // Set the email msg text.
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setText(bodyText);

            // Set the email attachment file
            FileDataSource fileDataSource = new FileDataSource(attachmentName);

            MimeBodyPart attachmentPart = new MimeBodyPart();
            attachmentPart.setDataHandler(new DataHandler(fileDataSource));
            attachmentPart.setFileName(fileDataSource.getName());

            // Create Multipart E-Mail.
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(attachmentPart);

            msg.setContent(multipart);
            Transport.send(msg, Config.emailUsername, Config.emailPassword);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

