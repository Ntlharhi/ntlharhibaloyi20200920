package Domain;

import java.util.ArrayList;
import java.util.List;

public class TestSuite {
    public String Name;
    public String Id;
    public String ProjectId;
    public String ExecutionDateTime;
    public String ExecutionTime;
    public String TestCaseCount;
    public String TotalPass;
    public String TotalFail;
    public String PassPercentage;
    public String FailPercentage;
    public List<TestCase> TestCases = new ArrayList<TestCase>();

    public String EnvironmentName;
    public List<String> BrowserName = new ArrayList<String>();
    public List<String> ScheduledScripts = new ArrayList<String>();
    public List<String> ScriptsNotRun = new ArrayList<String>();

    public boolean Status;
}
