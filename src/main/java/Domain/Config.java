package Domain;

import java.util.ArrayList;
import java.util.List;

public class Config {

    public static String Environment;
    public static List<String> Browser = new ArrayList<String>();
    public static boolean sendEmail =false;
    public static String emailFrom;
    public static List<String> emails = new ArrayList<String>();
    public static String emailUsername;
    public static String emailPassword;
    public static String emailHost;
    public static String emailPort;
}
