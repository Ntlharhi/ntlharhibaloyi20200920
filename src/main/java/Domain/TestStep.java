package Domain;

import java.util.ArrayList;
import java.util.List;

public class TestStep {

    public  String Id;
    public String TestCaseId;



    public int StepNumber;
    public String Description;
    public String Expected;
    public String Actual;
    public boolean Status;
    public List<Domain.Screenshot> Screenshot =new ArrayList<Domain.Screenshot>();
}
