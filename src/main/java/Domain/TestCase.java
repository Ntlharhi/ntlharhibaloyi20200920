package Domain;

import java.util.ArrayList;
import java.util.List;

public class TestCase {
    public String Id;
    public String TestSuiteId;
    public String Name;
    public boolean Status;
    public int QC_ID;
    public String ExecutionTime;
    public String Browser;
    public String RunOrder;
    public List<TestStep> TestSteps = new ArrayList<TestStep>();
}
