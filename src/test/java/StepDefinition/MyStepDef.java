package StepDefinition;

import Framework.Executor;
import Framework.TestSuiteManager;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyStepDef extends Executor {


    @Given("\"([^\"]*)\" is open with \"([^\"]*)\" on main page")
    public void ChromeONContactUs(String browser, String url) {
        NavigateTo(browser, url);
        SetTestCaseName(getScenario(), 123);
        SetStep("Navigate to registration page", "Navigated to registration page successfully");
        ReportItem("lnkSignUp", "xpath", "Page landed on the main website page");
    }


    @When("I click signup button, the website navigate to registration page")
    public void NavigateToRegistration()
    {
        SetStep("Click on signup link", "Website navigate to registration form");
        Click("lnkSignUp", "xpath", "Website navigated to registration page");
    }

    @Then("I completed all mandatory fields with existing data")
    public void CompleteFrom()//String name,String email,String mobile,String job,String company
    {
       SetStep("Enter values on the form ad submit", "Registration form submited successfully");
        Click("btnSubmit", "xpath", "Registration form fail to submit");
        ClearAndType("txtName","xpath","Lesetja Seanego","Filed to complate registraion form");
        ClearAndType("txtEmail","xpath","seanegolese@gmail.com","Filed to complate registraion form");
        ClearAndType("txtMobileNumber","xpath","0763180574","Filed to complate registraion form");
        ClearAndType("txtJobTitle","xpath","TTA","Filed to complate registraion form");
        ClearAndType("txtCompany","xpath","TTA TTA TTA","Filed to complate registraion form");
        Click("btnTermsAndCondition", "xpath", "Registration form fail to submit");
    }

    @Then("Registration return an error to show that data already exist")
    public void SubmitForm()
    {
        SetStep("Enter values on the form ad submit", "Registration form submited successfully");
        Click("btnNext", "xpath", "Registration form fail to submit");

    }


    /**
     * Set up Executor to use Scenario as Test Case Name
     */
    private static HashMap<Integer, String> scenarios;

    public MyStepDef() {
        if (scenarios == null)
            scenarios = new HashMap<Integer, String>();
    }

    @Before
    public void beforeHook(Scenario scenario) {
        addScenario(scenario.getName());
    }

    @After
    public void afterScenario() {
        TestSuiteManager.EndTest();
        driver.quit();
    }

    private void addScenario(String scenario) {
        Thread currentThread = Thread.currentThread();
        int threadID = currentThread.hashCode();
        scenarios.put(threadID, scenario);
    }

    private synchronized String getScenario() {
        Thread currentThread = Thread.currentThread();
        int threadID = currentThread.hashCode();
        return scenarios.get(threadID);
    }
}
