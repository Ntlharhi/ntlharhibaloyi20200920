package Platform.UI.Web;

import Framework.TestSuiteManager;
import cucumber.ExtendedCucumberRunner;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(features = {"src/test/java/Features/"},
        glue={"StepDefinition"},
        plugin = {
                "pretty",
                "json:target/courgette-report/courgette.json",
                "html:target/courgette-report/courgette.html"})

public class Runner{
    @BeforeSuite
    public static void setUp() {
        // TODO: Add your pre-processing

        System.out.println("Sart");
    }

    @AfterSuite
    public static void tearDown() {
        // TODO: Add your post-processing
        TestSuiteManager.tearDown();
    }
}
