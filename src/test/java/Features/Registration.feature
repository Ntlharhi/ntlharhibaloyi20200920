Feature: Registration process

  Scenario:  Verify registration process fail when data already exist

    Given "Chrome" is open with "https://uatweb.signinghub.co.za/" on main page
    When I click signup button, the website navigate to registration page
    When I completed all mandatory fields with existing data

    Then Registration return an error to show that data already exist